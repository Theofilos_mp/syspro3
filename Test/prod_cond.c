#include <stdio.h> 		// from www.mario-konrad.ch
#include <pthread.h>
#include <unistd.h>

#define POOL_SIZE 6

typedef struct {
	int data[POOL_SIZE];
	int start;
	int end;
	int count;
} pool_t;

int num_of_items = 15;
int producers = 0;
int consumers = 0;

pthread_mutex_t mtx1;
pool_t pool;

void initialize(pool_t * pool) {
	pool->start = 0;
	pool->end = -1;
	pool->count = 0;
}

void place(pool_t * pool, int data) {
	pthread_mutex_lock(&mtx1);
	while (pool->count >= POOL_SIZE || consumers > 0) {
		printf(">>p: Found Buffer Full count: %d c:%d\n", pool->count, consumers);
		pthread_cond_wait(&cond_nonfull, &mtx1);
	}

	data = num_of_items;
	pool->end = (pool->end + 1) % POOL_SIZE;
	pool->data[pool->end] = data;


	printf("Producer in cs end:%d in:%d \n", pool->end, data);

	num_of_items--;
	producers++;

	pool->count++;
	pthread_mutex_unlock(&mtx1);
}

int obtain(pool_t * pool) {
	int data = 0;
	pthread_mutex_lock(&mtx1);
	/* If a consumer removed all the elements return */
	printf("Before if %d, %d\n", pool->count, num_of_items);

	while (pool->count <= 0 || producers > 0) {
		printf(">>c: Found Buffer Empty c: %d p:%d\n", pool->count, producers);

		if (num_of_items <= 0 && pool->count == 0) {
			printf("Pool count %d\n", pool->count);
			pthread_mutex_unlock(&mtx1);	
			return 0;
		}

		pthread_cond_wait(&cond_nonempty,&mtx1);
	}
	data = pool->data[pool->start];
	pool->start = (pool->start + 1) % POOL_SIZE;

	printf("Consumer in cs st:%d out :%d pc: %d\n", pool->start, data, pool->count);
	consumers++;

	pool->count--;
	pthread_mutex_unlock(&mtx1);
	return data;
}

void * producer(void * ptr)
{
	int x = (*(int*)ptr);

	
	while (num_of_items > 0) {

		printf("porducer goest to place(%d)\n", x);
		place(&pool, num_of_items);
		printf("producer(%d): %d\n",x, num_of_items + 1);

		/* exit */
		pthread_mutex_lock(&mtx1);
		producers--;
		if (producers == 0){
			printf("Producer(%d) wakes up consumers\n", x);
			pthread_cond_broadcast(&cond_nonempty);
		}
		pthread_mutex_unlock(&mtx1);

		//usleep(1000);
	}

	printf("Producer(%d) out\n", x);
	pthread_exit(0);
}

void * consumer(void * ptr)
{
	int x = (*(int*)ptr);

	while (num_of_items > 0 || pool.count > 0) {

		printf("consumer goest to obtain(%d)\n", x);
		printf("consumer(%d): %d\n",x, obtain(&pool));

				/* exit */
		pthread_mutex_lock(&mtx1);
		consumers--;
		if (consumers == 0) {
			printf("Consumer(%d) wakes up producers\n", x);
			pthread_cond_broadcast(&cond_nonfull);
		}
		pthread_mutex_unlock(&mtx1);

		//usleep(500000);
	}

	printf("Consumer(%d) out\n", x);
	pthread_exit(0);
}

int main(int argc, char ** argv)
{
	pthread_t cons,cons2,cons3,cons4,cons5,cons6,cons7,cons8,cons9,
	          prod,prod2,prod3;
	
	initialize(&pool);
	pthread_mutex_init(&mtx1, 0);
	pthread_mutex_init(&mtx2, 0);
	pthread_mutex_init(&mtx3, 0);
	pthread_cond_init(&cond_nonempty, 0);
	pthread_cond_init(&cond_nonfull, 0);

	/*consm 3*/
	int x = 0;
	pthread_create(&cons, 0, consumer, &x);
	int y = 1;
	pthread_create(&cons2, 0, consumer, &y);
	int z = 2;
	pthread_create(&cons3, 0, consumer, &z);
	x = 3;
	pthread_create(&cons4, 0, consumer, &x);
	y = 4;
	pthread_create(&cons5, 0, consumer, &y);
	z = 5;
	pthread_create(&cons6, 0, consumer, &z);
	x = 6;
	pthread_create(&cons7, 0, consumer, &x);
	y = 7;
	pthread_create(&cons8, 0, consumer, &y);
	z = 8;
	pthread_create(&cons9, 0, consumer, &z);

	/*prod 3*/
	int p = 0;
	pthread_create(&prod, 0, producer, &p);
	int l = 1;
	pthread_create(&prod2, 0, producer, &l);
	int o = 2;
	pthread_create(&prod3, 0, producer, &o);


	pthread_join(prod, 0);
	pthread_join(prod2, 0);
	pthread_join(cons, 0);
	pthread_join(cons2, 0);
	pthread_cond_destroy(&cond_nonempty);
	pthread_cond_destroy(&cond_nonfull);
	pthread_mutex_destroy(&mtx1);
	pthread_mutex_destroy(&mtx2);
	pthread_mutex_destroy(&mtx3);
	return 0;
}