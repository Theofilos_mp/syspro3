#include "util.h"

void perror_exit(char *str){
	perror(str);
	exit(-1);
}

int lenfinder(int x) {
    /* -------------------------
     * Find the length of a num.
     * ------------------------- */

    if(x>=1000000000) return 10;
    if(x>=100000000) return 9;
    if(x>=10000000) return 8;
    if(x>=1000000) return 7;
    if(x>=100000) return 6;
    if(x>=10000) return 5;
    if(x>=1000) return 4;
    if(x>=100) return 3;
    if(x>=10) return 2;
    return 1;
}


void printCsInfo(void *csinfo) {
	csInfo_t *csi = (csInfo_t *) csinfo;
	printf("csinfo: %s %s %s %s\n", csi->csAddress,
                                    csi->csPort,
                                    csi->csDirOrFile,
                                    csi->csDelay
                                    );    
}

void freeCsInfo(void *csinfo) {
	csInfo_t *csi = (csInfo_t *) csinfo;
	free(csi->csAddress);
    free(csi->csPort);
    free(csi->csDirOrFile);
    free(csi->csDelay);
    free(csi);
}

csInfo_t *string2scInfo(char *string) {
	csInfo_t *csInfoPtr;
	char *token = NULL;

	/* Create new csInfo */
	csInfoPtr = malloc(sizeof(csInfo_t));
	if (csInfoPtr == NULL)
		perror_exit("Error when allocating mem");

  	/* tokenize string and get the info */
	token = strtok(string, ":");
	//printf("Token %s\n", token);

	/* Get the address*/
	csInfoPtr->csAddress = malloc((strlen(token) + 1) * sizeof(char));
	if (csInfoPtr->csAddress == NULL) 
		perror_exit("Error when allocating mem");
	strcpy(csInfoPtr->csAddress, token);

	/* Get the port */
	token = strtok(NULL, ":");
	//printf("Token %s\n", token);
	csInfoPtr->csPort = malloc((strlen(token) + 1) * sizeof(char));
	if (csInfoPtr->csPort == NULL) 
		perror_exit("Error when allocating mem");
	strcpy(csInfoPtr->csPort, token);

	/* Get the DirOdFile */
	token = strtok(NULL, ":");
	//printf("Token %s\n", token);
	csInfoPtr->csDirOrFile = malloc((strlen(token) + 1) * sizeof(char));
	if (csInfoPtr->csDirOrFile == NULL) 
		perror_exit("Error when allocating mem");
	strcpy(csInfoPtr->csDirOrFile, token);

	/* Get the Delay */
	token = strtok(NULL, ":");
	//printf("Token %s\n", token);
	csInfoPtr->csDelay = malloc((strlen(token) + 1) * sizeof(char));
	if (csInfoPtr->csDelay == NULL) 
		perror_exit("Error when allocating mem");
	strcpy(csInfoPtr->csDelay, token);

	return csInfoPtr;
}

char *readMsgFromSocket(int sockfd, msg_t **mes) {
	char *buffer;
	msg_t *msg = malloc(sizeof(msg_t));

	if (msg == NULL) perror_exit("Error when allocating mem");

	/* Read msg */
	if (read(sockfd, msg, sizeof(msg_t)) == -1)
		perror_exit("Error when reading msg");

	/* If len is 0 then msg has no body */
	if (msg->len == 0) {
		*mes = msg;
		return NULL;
	}

	/* Initialize the buffer */	
	buffer = malloc(sizeof(char) * msg->len);
	if (buffer == NULL)
		perror_exit("Error when allocating mem");

	/* Read the msg body */
	if (read(sockfd, buffer, sizeof(char) * msg->len) == -1)
		perror_exit("Error when reading msg");

	//printf("Server read \n%s\n len %d\n", buffer, msg->len);
	fflush(stdout);

	*mes = msg;
	return buffer;
}


int sendMsgToSocket(int sockfd, char * msg2str, char *type){
	msg_t msg;

	/* If msg2str is null then we send a simple info msg without body */
	if (msg2str != NULL)

		/* Initialize the msg len */
		msg.len = strlen(msg2str) + 1;
	else 
		msg.len = 0;

	/* Intilaize the msg type */
	strcpy(msg.str, type);
	msg.str[3] = '\0';

	/* First write the pre msg */
	if (write(sockfd, &msg, sizeof(msg_t)) < 0) {
		perror("Error when writing msg_t");
		return -1;
	}

	/* Then write the actual msg */
	if (msg2str != NULL)
		if (write(sockfd, msg2str, sizeof(char) * msg.len) < 0) {
			perror("Error when writing msg_t");
			return -1;
		}

	return 0;
}


char **threads_tokenize(char * string, char delim, int *arraySize) {
	/* --------------------------
	 * Returns an array with all 
     * the tokens tokens inside
     * string.
	 *-------------------------- */

     char ch = 'a', p_ch;
     char **array;
     int index = 0, delimCount = 0, tokenlen, startIndex, strindex;

     /* In case of empty string */
     if (string == NULL) return NULL;

     /* Loop once to find the delims num -> find the array size */
     while ((ch = string[index]) != '\0') {
     	if (ch == delim)
     		delimCount++;
     	index++;
     }

     /* Once you know how many delims tou have make the array */
     *arraySize = delimCount + 1;
     array = malloc(sizeof(char *) * (*arraySize));
     if (array == NULL) perror_exit("Error when allocating mem");

     //printf("IN tokenize %d\n", *arraySize);

	 /* Loop evry char of the string */
	 index = 0;
	 strindex = 0;
	 tokenlen = 0;
	 startIndex = 0;
	 ch = 'a';
	 p_ch = 'a';
	 while (ch != '\0') {
	 	ch = string[strindex];

	 	//printf("We read char %c\n", ch);

	 	/* If we found the delim or the string edded make the string */
	 	if (ch == delim || (ch == '\0' && p_ch != delim)) {

	 		array[index] = malloc(sizeof(char) * (tokenlen + 1));
			if (array[index] == NULL) 
				perror_exit("Error when allocating mem");

			strncpy(array[index], &string[startIndex], tokenlen);
			array[index][tokenlen] = '\0';

			//printf("Token :%s tokenlen %d , strlen %d\n",array[index], tokenlen, strlen(array[index]));

			tokenlen = -1;              // -1 becasue it will get add at the end of the loop
	 		startIndex = strindex + 1;  // For the next strindex 
	 		index++;                    // Count the array places
	 	}


	 	/* Update indecies */
	 	tokenlen++;  strindex++; p_ch = ch;
	 }

	 /* In case of a delim before the \0 we dont make a string 
	  * but here we have to mas the array smaller and deallocate 
	  */ 

	if (string[strlen(string) - 1] == delim) {

		*arraySize = *arraySize - 1;
	}

	//printf("String array size %d %s\n", *arraySize, string);
	//fflush(stdout);

	 return array;
}


void serverFailedPerror(char * err, int sockfd) {
	/* ------------------------------------
	 * Informs client about a an error that
	 * will make server halt.
	 * ------------------------------------ */

	sendMsgToSocket(sockfd, NULL, ERR);
	perror(err);
	exit(-1);
}



