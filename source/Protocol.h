#ifndef  __PROTOCOL_H__
#define  __PROTOCOL_H__


#define BUFSIZE 1024     // Hybrid protocol , with buffer size and msg lenght

/* Messages Special Chars */
#define FLS         '$'  // Feild separator
#define ERR        "err" // A server failed for some reason , that msg informs cilents
#define EOT        "eot" // States that no more messages of same type will be send.
						 // This is used only to messages of type LIST that have no 
						 // fixed lenght.

#define LEN         "len" // After a len msg there is a num .This num show how many char will follow.
#define CSI         "csi" // States that this is a content server info message
#define EOI         "eoi" // States that the iniator sent all csi infos

/* +---------------------------------------------------------+
 * | Message sent from MirrorManager thread to contentserver |
 * +---------------------------------------------------------+ */
#define LSM         "lsm"   // States that the MirrorManagers requests for a list of contents server available contents
#define FCM         "fcm"   // States that the msg body contains the file requested for fetch        

/* +-----------------------------------------------------+
 * | Messages sent from ContetntServer to Mirror manager |
 * +-----------------------------------------------------+ */
#define ATL              "atl" // States that the msg body is an answer to a LIST request
#define ANSWER_TO_FETCH  "atf" // States that the msg body contains the data of the file requested 
#define FNN              "fnn" // States that the file requested in fetch was not found (no msg body)

/* +-----------------------------------------------------+
 * | Messages sen from MirrorManager to mirror initiator | 
 * +-----------------------------------------------------+ */
#define SNF             "snf" // States that a server wasn't found
#define TRANSFER_ENDED  "ggt" // States that the transfer was succsesfull       

/* +-----------------------------------------------------+
 * | Messages sen from mirror initiator to content server| 
 * +-----------------------------------------------------+ */
#define CLOSE           "cls" // Close the server


/* Threads return ints */
#define CS_NOTFOUND   -2  // States tha contet server not found


/* Struct len msg */
typedef struct tagmsg {
	char  str[4];   // all the msgs are a 3 char strings
	int   len;      /* lenght of the msg that follows. If greater than 
	                 * buffer size then we are waiting for more msgs.
	                 * Else this is the only one 
                     */
} msg_t;

#endif