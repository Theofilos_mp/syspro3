#ifndef __SERVER_FUNCTIONS_H__
#define __SERVER_FUNCTIONS_H__ 

#include <stdio.h>
#include <sys/stat.h>
#include <sys/wait.h> /* sockets */
#include <sys/types.h> /* sockets */
#include <sys/socket.h> /* sockets */
#include <netinet/in.h> /* internet sockets */
#include <netdb.h> /* gethostbyaddr */
#include <unistd.h> /* fork */
#include <stdlib.h> /* exit */
#include <ctype.h> /* toupper */
#include <signal.h> /* signal */
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <dirent.h>  /* Directorys */ 

#include "queue.h"
#include "Protocol.h"
#include "util.h"
#include "threads.h"


/* Mirror server Functions */

/* +------------------------------------+
 * | This function sends statistics     |
 * | kept in @syncroBlock to the client |
 * | listening on @fd                   |
 * +------------------------------------+ */
void send_statistics(int fd, mirrors_SyncroBlock *syncroBlock, int contentServers, int contentServersNotFound);

void sendCsNotFound(int fd, csInfo_t *csinfoptr);
pthread_t *createWorkers(int threadNum, mirrors_SyncroBlock *syncroBlock, char * dirName, int sockfd) ;
pthread_t *createMirrorManagers(queue_t *requestQueue, mirrors_SyncroBlock *syncroBlock);
void getCServersInfo(int sockfd, char *request, queue_t *requestQueue);


/* Content server functions */
void sendListOfContents(struct connHandlerArgs *tInfo, char *rcvmsg);
void sendRequestedContent(struct connHandlerArgs *tInfo, char *rcvmsg);

#endif