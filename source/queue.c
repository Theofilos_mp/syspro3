//
// Created by theofilos on 4/1/2017.
//

#include "queue.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


queue_t* queue_create(void) {
    /* ------------------------------
     * Create the queue , returns NULL
     * in case of fault.
     * ------------------------------ */

    queue_t *queue;
    queue = malloc(sizeof(queue_t));
    if (queue == NULL) {
        perror("Allocating");
        return NULL;
    }

    /* Initialize queue */
    queue->contained_nodes = 0;
    queue->first = NULL;
    queue->last = NULL;

    //printf("Queue created!\n");
    return queue;
}

void queue_delete(queue_t *queue, void (*deleteData)(void *)) {
    /* ------------------------------
     * Delete the queue , and all
     * the groups seated at that time
     * ------------------------------ */

    queue_node_t *temp_node = queue->first, *temp_node2 = NULL;

    /* Delete everything */
    while(temp_node != NULL) {
        temp_node2 = temp_node->next;

        /* CHANGE IN REUSE */
        if (deleteData != NULL)
            deleteData(temp_node->data);
        
        /*-------------*/

        free(temp_node);
        temp_node = temp_node2;
    }
    free(queue);
    //printf("Queue deleted!\n");
}

int queue_is_empty(queue_t *queue){
    return queue->contained_nodes == 0;
}

int queue_add_node(queue_t* queue, void *data) {
    /* -----------------------------------
     * Returns  1 : Pid is added to queue
     * ----------------------------------- */

    /* Add the pid to the queue */
    queue_node_t *new_queue_node;
    new_queue_node = malloc(sizeof(queue_node_t));
    if (new_queue_node == NULL) {
        perror("error ");
        exit(-1);
    }

    /* Initialize queue node */
    new_queue_node->next = NULL;

    /* Add the pid at the end of the queue */
    if (queue->first == NULL) {
        queue->first = new_queue_node;
        queue->last = new_queue_node;
    } else {
        queue->last->next = new_queue_node;
        queue->last = new_queue_node;
    }
    queue->contained_nodes++;

    /* CHANGE IN REUSE */
    new_queue_node->data = data;
    /*-----------------*/

    return 1;
}

void *queue_pop(queue_t *queue) {
    /* ------------------------
     * Returns the first pid
     * and delete it from queue.
     * Returns -1 if empty.
     * ------------------------ */

    if (queue->contained_nodes == 0){
        return NULL;
    }

    /* Remove first node */
    queue_node_t *temp_node = queue->first;

    /* In case queue is size 1 */
    if (queue->first == queue->last) {
        queue->first = NULL;
        queue->last = NULL;
    } else {
        queue->first = queue->first->next;
    }

    /* CHANGE IN REUSE */
    void *rp;
    rp = temp_node->data;
    /*-----------------*/

    queue->contained_nodes--;
    free(temp_node);

    return rp;
}

void *queue_get(queue_t *queue, int index) {
    /* -------------------
     * Returns the node in
     * index's place of the
     * list
     * ------------------- */

    int i = 0;
    queue_node_t *temp_node = queue->first;

    while(temp_node != NULL && i != index) {
        i++;
        temp_node = temp_node->next;
    }

    
    if (temp_node == NULL) {
        return NULL;
    }

    return (void *) temp_node->data;
}

void queue_node_print(queue_node_t *queue_node) {

    /* CHANGE IN REUSE */
    /*-----------------*/

}

void queue_print(queue_t *queue, void (*printfun)(void *)) {

    queue_node_t *temp_node = queue->first;
    printf("Printing queue :\n");
    while(temp_node != NULL) {
        printf("->");
        (*printfun)(temp_node->data);
        temp_node = temp_node->next;
    }
}
