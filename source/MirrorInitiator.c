#include <stdio.h>
#include <sys/wait.h> /* sockets */
#include <sys/types.h> /* sockets */
#include <sys/socket.h> /* sockets */
#include <netinet/in.h> /* internet sockets */
#include <netdb.h> /* gethostbyaddr */
#include <unistd.h> /* fork */
#include <stdlib.h> /* exit */
#include <ctype.h> /* toupper */
#include <signal.h> /* signal */
#include <string.h>

#include "Protocol.h"
#include "queue.h"
#include "util.h"

int main(int argc, char const *argv[])
{

	int  sockfd, csiStrsNum, msglen = 0;
	char *server_ip, *server_port,  **strArray;
	struct addrinfo hints, *res;

   /* Handle the args of the program */
    int i, nflag = 0, pflag = 0, sflag = 0, sgetflag = 0, soptions = 0, purgeFlag = 0;
    char *temp;
    for ( i = 1; i < argc; i++) {  /* last arg is not an non option arg */
        if (i + 1 != argc) {
	        /*make sure that after option arg -l there is no other option arg*/
	        if (strcmp( argv[i], "-n") == 0 && argv[i+1][0] != '-') {
	        	server_ip = malloc((strlen(argv[i+1]) + 1)* sizeof(char));
	        	if (server_ip == NULL) {
	        		perror("Error ");
	        		exit(-1);
	        	}
	            strcpy(server_ip, argv[i+1]);
	            nflag = 1;
	        } 
	        else if (strcmp( argv[i], "-p") == 0 && argv[i+1][0] != '-') {

	            server_port = malloc((strlen(argv[i+1]) + 1)* sizeof(char));
	        	if (server_port == NULL) {
	        		perror("Error ");
	        		exit(-1);
	        	}
	            strcpy(server_port, argv[i+1]);
	            pflag = 1;				      
	        }
	        else if (strcmp( argv[i], "--purge") == 0 ) {

	        	/* optional arg for closing all servers */
	        	purgeFlag = 1;			      
	        }
	        else if (strcmp( argv[i], "-s") == 0 && argv[i+1][0] != '-') {

	        	/* Callculate number of Content Server Infos */
	        	soptions = 0; /* 6 six are the oprions and args apart from content server info */
	        	csiStrsNum = argc - (6 + purgeFlag);

	        	/* make an str array to hold all the strings of th csinfo for the request */
				strArray = malloc(sizeof(char *) * csiStrsNum);
				if (strArray == NULL) 
					perror_exit("Error when allocating mem");

	        	/* Update flags */
	            sflag = 1;
	            sgetflag = 1;
	            continue;
	        }
    	}

    	/* Get the -s info for the CServers */
        if (sgetflag && soptions < csiStrsNum) {

        	temp = malloc(strlen(argv[i]) + 1);
        	if (temp == NULL) {
        		perror("Error");
        		exit(-1);
        	}
        	strcpy(temp, argv[i]);
        	msglen += strlen(temp);

        	/* Keep thr strings on an array */
        	strArray[soptions] = temp;
        	soptions++;
        }
    }

    /* Check if the right arguments where given */
    if (!nflag || !pflag || !sflag) {
    	fprintf(stderr, "Invalud arguments, try : ./MirrorInitiator -n <MirrorServerAddress> -p <MirrorServerPort> \n\t-s <ContentServerAddress1:ContentServerPort1:dirorfile1:delay1, \n\tContentServerAddress2:ContentServerPort2:dirorfile2:delay2, ...>\n");
    	exit(-1);
    }

    /* +---------------+
	 * | CONNCET WITH  |
	 * | MIRROR SERVER |
	 * +---------------+ */

	/* Fill in some of the addrinfo fields */
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	/* Get server's ip */
	getaddrinfo(server_ip, server_port, &hints, &res);

	/* Create the socket */
	if ((sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) < 0)
		perror("socket");

	/* Connect to server */
	if (connect(sockfd, res->ai_addr, res->ai_addrlen) < 0) {

		/* Frees and exit*/
		for (i = 0; i < csiStrsNum; ++i){
			free(strArray[i]);
		}
		free(strArray);
		free(res);
		free(server_ip);
		free(server_port);
		close(sockfd);
		perror_exit("Can't connect with mirror server");
	}

	/* Loop to send the data over the TCP Connection */
	char  *msg;
	int   index = 0;

	/* Add all the strings of strArray to one msg */
	msg = malloc(sizeof(char) * (msglen + 1));
	if (msg == NULL) 
		perror_exit("Error when allocating mem");

	strcpy(msg, strArray[0]);
	for (index = 1; index < csiStrsNum; index++) {
		strcat(msg, strArray[index]);
	}
	msg[msglen] = '\0';

	printf("The str is %s len %d, accounted %d\n", msg, strlen(msg), msglen);

	/* Write the request to the server */
	sendMsgToSocket(sockfd, msg, CSI);

	/* +------------------+
	 * | Wait for answers |
	 * +------------------+ */
	printf("Waiting for statistics (and infos)...\n");
	msg_t *mes;
	char *buff;
	while (1) {


		buff = readMsgFromSocket(sockfd, &mes);

		/* Take actions depending on msg you got */
		if (strcmp(mes->str, SNF) == 0) {

			/* +-----------+
			 * | ERROR MSG |
			 * +-----------+ */
			printf("[ERROR] Content server \"%s\" not found \n", buff);
		}
		else if (strcmp(mes->str, FNN) == 0) {

			/* +-----------+
			 * | ERROR MSG |
			 * +-----------+ */
			char **tokens;
			int  arraySize;

			/* Tokenize the buffer */
			tokens = threads_tokenize(buff, FLS, &arraySize);

			printf("[ERROR] File \"%s\" not found in content server \"%s\"\n", tokens[1], tokens[0]);
			free(tokens[0]);  free(tokens[1]);  free(tokens);
		}
		else if (strcmp(mes->str, TRANSFER_ENDED) == 0) {

			/* +--------------+
			 * | ALL GOOD MSG |
			 * +--------------+ */
			char **tokens;
			int  arraySize, i;

			tokens = threads_tokenize(buff, FLS, &arraySize);

			printf("[Transfer done] Printing statistics:\n");
			for (i = 0; i < arraySize; i++) {
				printf("\t%s\n", tokens[i]);
				free(tokens[i]);
			}

			free(tokens);
			free(buff);
			free(mes);
			break;
		}
		else if (strcmp(mes->str, ERR) == 0) {
			/* +-----------+
			 * | ERROR MSG |
			 * +-----------+ */
			printf("[ERROR] Mirror server terminated abnormaly\n");
		}

		free(buff);
		free(mes);
	}


	/* After statatistics if purge flag is on purge content servers */
	if (purgeFlag) {
		printf("PURGING ...\n");

		close(sockfd);
		/* Fill in some of the addrinfo fields */
		memset(&hints, 0, sizeof(hints));
		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_STREAM;

		/* Get server's ip */
		getaddrinfo(server_ip, server_port, &hints, &res);

		/* Create the socket */
		if ((sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) < 0)
			perror("socket");

		/* First close mirror server . Connect again to send him a close msg */
		if (connect(sockfd, res->ai_addr, res->ai_addrlen) < 0) {

			/* Frees and exit*/
			for (i = 0; i < csiStrsNum; ++i){
				free(strArray[i]);
			}
			free(strArray);
			free(res);
			free(server_ip);
			free(server_port);
			close(sockfd);
			perror_exit("Can't connect with mirror server");
		}

		/* Close the Mirror Server */
		sendMsgToSocket(sockfd, NULL, CLOSE);

		/* +----------------+
		 * | For each       |
		 * | Content Server |
		 * +----------------+ */
		char **tokens;
		int arraySize;
		for (i=0; i < csiStrsNum; i++) {

			tokens = threads_tokenize(strArray[i], ':', &arraySize);

			free(res);
			memset(&hints, 0, sizeof(hints));
			hints.ai_family = AF_INET;
			hints.ai_socktype = SOCK_STREAM;

			/* Get server's ip */
			if (getaddrinfo(tokens[0], tokens[1], &hints, &res) != 0 ) {

				fprintf(stderr, "[ERROR] Can't close content server %s:%s", tokens[0], tokens[1]);
				perror(" ");

				for (index = 0; index < arraySize; index++)
					free(tokens[index]);
				free(tokens);

				continue;
			}

			/* Create the socket */
			close(sockfd);
			if ((sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) < 0)
				perror("socket");

			/* Connect to server */
			if (connect(sockfd, res->ai_addr, res->ai_addrlen) < 0) {
				fprintf(stderr, "[ERROR] Can't close content server %s:%s", tokens[0], tokens[1]);
				perror(" ");
			}
			else {
				sendMsgToSocket(sockfd, NULL, CLOSE);
			}

			for (index = 0; index < arraySize; index++)
				free(tokens[index]);
			free(tokens);
		}

		printf("[DONE]\n");
	}


	/* +-------+
	 * | FREES |
	 * +-------+ */
	free(msg);
	for (i=0; i < csiStrsNum; i++)
		free(strArray[i]);
	free(strArray);
	free(res);
	free(server_ip);
	free(server_port);
	close(sockfd);
	return 0;
}