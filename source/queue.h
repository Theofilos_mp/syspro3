//
// Created by theofilos on 4/1/2017.
//

#ifndef ASKHSH3DELHS_QUEUE_H
#define ASKHSH3DELHS_QUEUE_H

#include "Protocol.h"
#include <sys/types.h>

typedef struct queue_node_t {
    struct   queue_node_t *next;
    void     *data;   
} queue_node_t;


typedef struct queue_t {
    queue_node_t *first;
    queue_node_t *last;
    int contained_nodes;
} queue_t;


queue_t* queue_create(void);
void queue_delete(queue_t *queue, void (*deleteData)(void *));
int queue_add_node(queue_t *queue, void *);
int queue_is_empty(queue_t *queue);
void *queue_pop(queue_t *queue);
void *queue_get(queue_t *queue, int index);
void queue_print(queue_t *queue, void (*printfun)(void *));


#endif //ASKHSH3DELHS_QUEUE_H
