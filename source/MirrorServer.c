#define _GNU_SOURCE
#include <stdio.h>
#include <sys/stat.h>
#include <sys/wait.h> /* sockets */
#include <sys/types.h> /* sockets */
#include <sys/socket.h> /* sockets */
#include <netinet/in.h> /* internet sockets */
#include <netdb.h> /* gethostbyaddr */
#include <unistd.h> /* fork */
#include <stdlib.h> /* exit */
#include <ctype.h> /* toupper */
#include <signal.h> /* signal */
#include <string.h>
#include <pthread.h>
#include <errno.h>

#include "queue.h"
#include "threads.h"

/* Macros */
#define perror2(s,e) fprintf(stderr,"%s: %s\n",s,strerror(e))

/* A flag that kills the server */
int killLoop = 0;

/* Signal Handler function */
void sighdl (int sig) {
	killLoop = 1;
}



void       getCServersInfo(int sockfd, queue_t *queue);
pthread_t *createMirrorManagers(queue_t *requestQueue, mirrors_SyncroBlock *syncroBlock);
pthread_t *createWorkers(int threadNum, mirrors_SyncroBlock *syncroBlock, char * dir, int sockfd);
void       sendCsNotFound(int fd, csInfo_t *csinfoptr);
void       send_statistics(int fd, mirrors_SyncroBlock *syncroBlock, int csFound, int csNotFound);

int main(int argc, char *argv[]) {

	int      threadNum;
	char     c, *dirName;

	int port, sockfd, newsock;
	struct sockaddr_in server, client;
	socklen_t clientlen;
	struct sockaddr *serverptr=(struct sockaddr *)&server;
	struct sockaddr *clientptr=(struct sockaddr *)&client;
	struct hostent *rem;

	/* Get arguments */
	int pflag = 0, mflag = 0, wflag = 0;
	while ((c = getopt (argc, argv, "p:m:w:")) != -1)
	    switch (c)
	      {
	      case 'p':
	        pflag = 1;

	        /* Get the prot num */
	        port = atoi(optarg);
	        break;
	      case 'm':
	        mflag = 1;

	        /* Get the dir */
	        if((dirName = malloc(strlen(optarg) * (sizeof(char) + 1))) == NULL){
	        	perror("Error:");
	        	exit(-1);
	        }
	        strcpy(dirName, optarg);

	        /* Create dir in case it doesnt exist */
	        if (mkdir(dirName, 0777) == -1)
	        	if (errno != EEXIST)
	        		perror_exit("Error when creating Directory to save contents");

	        break;
	      case 'w':
	        wflag = 1;

	        /* Get thread num */
	        threadNum = atoi(optarg);
	        break;
	      case '?':
	        if (optopt == 'p' || optopt == 'm' || optopt == 'w')
	        	fprintf (stderr, "Option -%c requires an argument.\n", optopt);
	        else if (isprint (optopt))
	        	fprintf (stderr, "Unknown option `-%c'.\n", optopt);
	        else
	          fprintf (stderr,
	                   "Unknown option character `\\x%x'.\n",
	                   optopt);
	        return 1;
	      default:
	        abort ();
    	  }

   	if ( !wflag || !mflag || !pflag) {
   		fprintf(stderr, "Invalid arguments, try : ./MirrorServer -p <port> -m <dirname> -w <threadnum>\n");
   		exit(-1);
   	}


   	/* +----------------------+
  	 * | START SIGNAL HANDLER |
   	 * +----------------------+ */
	static struct sigaction act;
	act.sa_handler =  sighdl;      // Give our founction 
	sigfillset (&(act.sa_mask));
	sigaction (SIGTERM, &act, NULL);


	/* +----------------------+ 
     * | START UP CONNECTIONS |
	 * +----------------------+ */
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(-1);
	}

	/* Enable reusing socket */
	int yes = 1;
	if ( setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1 )
	{
	    perror("setsockopt");
	}

	/* Non blocking socket */
	fcntl(sockfd, F_SETFL, O_NONBLOCK);

	server.sin_family = AF_INET; /* Internet domain */
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_port = htons(port); /* The given port */

	/* Bind socket to address */
	if (bind(sockfd, serverptr, sizeof(server)) < 0){
		perror("bind");
		exit(-1);
	}

	/* Listen for connections */
	if (listen(sockfd, 5) < 0) {
		perror("listen");
		exit(-1);
	}
	printf("Listening for connections to port %d\n", port);

	/* Make the shared data for content server thread suncronaization */
	contents_SyncroBlock *csBlock = cs_syncroBlock_init();


	/* Loop and accept connections */
	int err;
	struct connHandlerArgs *arg;
	pthread_t th;
	queue_t          *threadsId_q = queue_create();
	while (!killLoop) {

		clientlen = sizeof(client);

		/* accept connection */
		if ((newsock = accept(sockfd, clientptr, &clientlen)) < 0) {

			/* If the error is beacause of non blocking accept then loop , else fail with perror_exit */
			if (errno == EAGAIN || errno == EWOULDBLOCK) {
				continue;
			}
			else 
				perror_exit("Error in accept");
		}
			
		/* Find client's address */
		if ((rem = gethostbyaddr((char *) &client.sin_addr.s_addr, sizeof(client.sin_addr.s_addr), client.sin_family))== NULL) {
			herror("gethostbyaddr"); exit(1);
		}

		printf("Accepted connection from %s\n", rem->h_name);

		/* Create a thread for each connection */
		arg = malloc(sizeof(struct connHandlerArgs));
		arg->sockfd    = newsock;
		arg->dirptr    = dirName;
		arg->block     = csBlock; 
		arg->threadNum = threadNum;
		if ((err = pthread_create(&th, NULL, connectionHandler, (void *) arg)) != 0) {
			perror2("Error when creating thread", err); 
			exit(-1);
		}

		queue_add_node(threadsId_q, (void *)th);
	}

	/* Join threads */
	int *ret_val = NULL;
	while ((void *) (th = (pthread_t) queue_pop(threadsId_q)) != NULL) {

		if (pthread_join(th, (void **) &ret_val) != 0)
			continue;

		/* In case of a erroneus return send msg to initiator */
		if (ret_val == NULL) {
			continue;
		}
		free(ret_val);
	}

	queue_delete(threadsId_q, NULL);
	delete_cs_syncroBlock(csBlock);
	free(dirName);
	return 0;
}
