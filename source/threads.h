#ifndef __THREADS_H
#define __THREADS_H

#include <stdio.h>
#include <sys/wait.h> /* sockets */
#include <sys/types.h> /* sockets */
#include <sys/socket.h> /* sockets */
#include <netinet/in.h> /* internet sockets */
#include <netdb.h> /* gethostbyaddr */
#include <unistd.h> /* fork */
#include <stdlib.h> /* exit */
#include <ctype.h> /* toupper */
#include <string.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>


#include "queue.h"
#include "Protocol.h"
#include "util.h"
#include "queue.h"

/* Macros */
#define perror2(s,e) fprintf(stderr,"%s: %s\n",s,strerror(e))

#define POOL_SIZE 5                         // The fixed size of the queue (shared buff)


/* Shared buffers data  */
typedef struct {
	char *cs_ip;
	char *cs_port;
	char *cs_dirOrFile; 
	char *cs_id;
}cs_data_t;


/* Shared buffer for the threads */
typedef struct {
	cs_data_t *data[POOL_SIZE];
	int       start;
	int       end;
	int       count;
} pool_t;

/* +-------------------+
 * | STRUCT FOR THREAD |
 * |  SYNCONAZATION    |
 * | FOR MIRROR SERVER |   
 * +-------------------+ */
typedef struct {
	pthread_mutex_t mtx;                  // The mutex to syncronize threads in pool
	pthread_mutex_t statistics_mtx;       // The mutext to syncornize workers acces to statistic variables
	pthread_cond_t  cond_nonempty;        // Cond variable for a non empty queue
	pthread_cond_t  cond_nonfull;         // Cond variable for a non full queue
	int             killWorkers;                            // A flag that kills workers when its on

	/* Statistics vars (protected by statistics_mtx) */
	int             bytesTransfered;
	int             filesTransfered;
	int             foldersTransfered;
	int             sumOfSquares;

	/* The shared queue */
	pool_t          *pool;
} mirrors_SyncroBlock;

/* +--------------------+
 * | STRUCT FOR THREAD  |
 * |  SYNCONAZATION     |
 * | FOR CONTENT SERVER |   
 * +--------------------+ */
typedef struct {
	pthread_mutex_t mtx; 
	pthread_cond_t  readers_cond, writers_cond;
	int             readers, writer;
	queue_t         *delayANDid_q;
} contents_SyncroBlock;

/* Queue Data for readers/wirters problem's queue */
typedef struct {
	char *contId;
	int  delay;
} queue_data;


/* Worker arguments */
typedef struct {
	char              *dir;
	int               initiator_fd;
	mirrors_SyncroBlock *syncroData;
} workerArgs;

/* Manager arguments */
typedef struct {
	csInfo_t          *csinfo;
	mirrors_SyncroBlock *syncroData;
}managerArgs;

/* Connection Handlers arguments */
struct connHandlerArgs {
	int        sockfd;
	int        threadNum;
	char       *dirptr;
	contents_SyncroBlock *block;
};


/* Thread functions */
void *mirrorManager(void *arg);
void *worker(void *arg);
void *connectionHandler(void *arg);

/* Initalize the global vars for thread syncronization */
mirrors_SyncroBlock   *syncroData_init(void);
contents_SyncroBlock  *cs_syncroBlock_init(void);
void                  delete_syncroBlock(mirrors_SyncroBlock *syncroBlock);
void                  delete_cs_syncroBlock(contents_SyncroBlock *block);

/* Give main a wait to kill workers when everything is done */
void funckillWorkers();

#endif