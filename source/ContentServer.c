#include <stdio.h>
#include <sys/wait.h> /* sockets */
#include <sys/types.h> /* sockets */
#include <sys/socket.h> /* sockets */
#include <netinet/in.h> /* internet sockets */
#include <netdb.h>   /* gethostbyaddr */
#include <unistd.h>  /* fork */
#include <stdlib.h>  /* exit */
#include <ctype.h>   /* toupper */
#include <signal.h>  /* signal */
#include <string.h>
#include <dirent.h>  /* Directorys */ 
#include <pthread.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "queue.h"
#include "util.h"
#include "Protocol.h"



/* Macros */
#define perror2(s,e) fprintf(stderr,"%s: %s\n",s,strerror(e))

pthread_mutex_t mtx; 
pthread_cond_t  readers_cond, writers_cond;
int             readers, writer;


int killLoop = 0;

/* Queue Data for readers/wirters problem's queue */
typedef struct {
	char *contId;
	int  delay;
} queue_data;

void delete_queueData(void *q_data){
	queue_data *data = (queue_data *) q_data;
	free(data->contId);
	free(data);
}

/* Argument for threads */
struct threadArg
{
	int        sockfd;
	const char *dirptr;   /* Can't change this (all threads get the pointer only ) */
	queue_t    *conn_info;
};

/* Thread function */
void *connectionHandler(void *arg);

/* Signal function */
void sighdl (int sig) {
	killLoop = 1;
}


int main(int argc, char *argv[])
{
	int                 port, sockfd, newsock;  // Our port , and socket fds
	DIR                 *contentDir;
	char                *dirName;
	struct sockaddr_in  server, client;
	socklen_t           clientlen;
	struct sockaddr     *serverptr = (struct sockaddr *)&server;
	struct sockaddr     *clientptr = (struct sockaddr *)&client;
	struct hostent      *rem;  // A struct to keep the info of the client
	queue_t             *connectionInfo;

	/* +---------------------+
	 * | Handle the programs |
	 * |     arguments       | 
	 * +---------------------+ */
	int pflag = 0, dflag = 0;
	char c;
	while ((c = getopt(argc, argv, "p:d:")) != -1)
		switch (c)
		{
			case 'd':
				dflag = 1;

				/* Open dir */
				contentDir = opendir(optarg);
				if (contentDir == NULL)
					perror_exit("Error with given dirname");
				closedir(contentDir);

				 /* Get the dir */
		        if((dirName = malloc(strlen(optarg) * (sizeof(char) + 1))) == NULL){
		        	perror("Error:");
		        	exit(-1);
		        }
		        strcpy(dirName, optarg);

		        if (dirName[strlen(dirName)] == '/')
		        	dirName[strlen(dirName)] = '\0';

				break;
			case 'p':
				pflag = 1;

				/* Get the port num */
				port = atoi(optarg);
				break;
			case '?':
		        if (optopt == 'p' || optopt == 'm' || optopt == 'w')
		        	fprintf (stderr, "Option -%c requires an argument.\n", optopt);
		        else if (isprint(optopt))
		        	fprintf (stderr, "Unknown option `-%c'.\n", optopt);
		        else
		          fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
		        return 1;
		    default:
		       abort();	    	  
		}

	if (!pflag || !dflag)  {
		printf("Invalide arguments, try : ./ContentServer -p <port> -d <dirorfilename>\n");
		exit(-1);
	}


	/* Initialize siganl handler */
	static struct sigaction act;
	act.sa_handler =  sighdl;
	sigfillset (&(act.sa_mask));
	sigaction (SIGTERM, &act, NULL);


	/* Initliaze sockaddr_in struct for server */
	server.sin_family = AF_INET;
	server.sin_port   = htons(port);
	server.sin_addr.s_addr = htonl(INADDR_ANY);

	/* Create a socket */
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		perror_exit("Error in socket");

	/* Reuse socket */
	int yes = 1;
	if ( setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1 )
	{
	    perror("setsockopt");
	}

	/* Non blocking socket */
	fcntl(sockfd, F_SETFL, O_NONBLOCK);

	/* Bind to that socket */
	if (bind(sockfd, serverptr, sizeof(server)) < 0) 
		perror_exit("Error in bind");

	/* Listen for connections */
	if (listen(sockfd, 5) < 0)
		perror_exit("Error in listen");

	printf("Listening for connections to port %d\n", port);

	/* Intialize connection info and global vars*/
	connectionInfo = queue_create();
	pthread_mutex_init(&mtx, 0);
	pthread_cond_init(&writers_cond, NULL); 
	pthread_cond_init(&readers_cond, NULL); 

	struct threadArg *arg;
	pthread_t th;
	queue_t          *threadsId_q = queue_create();
	int err;
	while (!killLoop) {

		clientlen = sizeof(client);

		/* accept connection */
		if ((newsock = accept(sockfd, clientptr, &clientlen)) < 0) {

			/* If the error is beacause of non blocking accept then loop , else fail with perror_exit */
			if (errno == EAGAIN || errno == EWOULDBLOCK) {
				continue;
			}
			else 
				perror_exit("Error in accept");
		}
			

		/* Find client's address */
		if ((rem = gethostbyaddr((char *) &client.sin_addr.s_addr, sizeof(client.sin_addr.s_addr), client.sin_family))== NULL) {
			herror("gethostbyaddr"); exit(1);
		}

		printf("Accepted connection from %s\n", rem->h_name);

		/* Create a thread for each connection */
		arg = malloc(sizeof(struct threadArg));
		arg->sockfd    = newsock;
		arg->dirptr    = dirName;
		arg->conn_info = connectionInfo; 
		if ((err = pthread_create(&th, NULL, connectionHandler, (void *) arg)) != 0) {
			perror2("Error when creating thread", err); 
			exit(-1);
		}

		queue_add_node(threadsId_q, (void *)th);

	}

	/* Join threads */
	int *ret_val = NULL;
	while ((void *) (th = (pthread_t) queue_pop(threadsId_q)) != NULL) {

		if (pthread_join(th, (void **) &ret_val) != 0)
			continue;

		/* In case of a erroneus return send msg to initiator */
		if (ret_val == NULL) {
			continue;
		}

		free(ret_val);
	}

	queue_delete(threadsId_q, NULL);
	queue_delete(connectionInfo, delete_queueData);
	free(dirName);
	return 0;
}

/* +-----------------+
 * | READER / WRITER |
 * |   FUNCTIONS     |
 * +-----------------+ */
 void queueWrite(queue_t *queue, queue_data *data) {
 	/* --------------------------------------
 	 * This function writes to the queue the 
 	 * tuple of <id, delay> .
 	 * -------------------------------------- */

 	pthread_mutex_lock(&mtx);

 	while (writer || readers >0) {
 		pthread_cond_wait(&writers_cond, &mtx);
 	}
 	writer = 1;
	pthread_mutex_unlock(&mtx);

 	/* Write the data */
 	queue_add_node(queue, (void *) data);

 	pthread_mutex_lock(&mtx);
 	writer = 0;
 	pthread_cond_broadcast(&readers_cond); // wake up all readers
 	pthread_cond_signal(&writers_cond);    // wake up a writer
 	pthread_mutex_unlock(&mtx);

 	return;
 }

int readAndCompare(queue_t *queue, char *contentId) {
	/* --------------------------------------
 	 * This function reads the queue and returns 
 	 * the id , if the argument contentId exists
 	 * int a tuple of the queue.
 	 * -------------------------------------- */
 	int i, ret_val = 1;  // default delay time in case it wasent found 
 	queue_data *data;

 	pthread_mutex_lock(&mtx);

 	while (writer) {
 		pthread_cond_wait(&readers_cond, &mtx);
 	}
 	readers++;
	pthread_mutex_unlock(&mtx);

 	/* Read all queue */
	for (i = 0; (data = queue_get(queue, i)) != NULL; i++) {
		if (strcmp(data->contId, contentId) == 0) 
			ret_val = data->delay;
	}
 	

 	pthread_mutex_lock(&mtx);
 	readers--;
 	if (readers == 0)
 		pthread_cond_signal(&writers_cond);    // wake up a writer
 	pthread_mutex_unlock(&mtx);

 	return ret_val;
}



/* +--------------+
 * | THREAD FUNCS |
 * +--------------+ */
void sendRequestedContent(struct threadArg *tInfo, char *rcvmsg) {
	/* --------------------------------------------
	 * Sends to the socketfd the requested info .
	 * In case of a file not found send error msg.
	 * -------------------------------------------- */
	char *buffer, *requestedPath, *contentId, **tokens;
	int  filefd, fileSize, loops, readBytes, sockfd, arraySize;

	/* Tokenize recived msg and get content id and requested file */
	tokens = threads_tokenize(rcvmsg, FLS, &arraySize);
	contentId     = tokens[0];
	requestedPath = tokens[1];
	free(tokens);

	//printf("Read %s %s\n",contentId, requestedPath);

	sockfd = tInfo->sockfd; 

	/* Check if we have that file on our server */
	if ((filefd = open(requestedPath, O_RDONLY) ) == -1 ) {
		perror("Requested file open problem");
		sendMsgToSocket(sockfd, NULL, FNN);

		free(contentId);  free(requestedPath);
		return ;
	}

	/* Wait the delay time */
	sleep(readAndCompare(tInfo->conn_info, contentId));

	/* +-------------------------+
	 * | Send the file's content |
	 * | to mirror manager       |
	 * +-------------------------+ */
	fileSize = (int) lseek(filefd, 0, SEEK_END); 
	//printf("File has %d bytes\n", fileSize);

	/* Intialize buffer */
	buffer = malloc(sizeof(char) * BUFSIZE);
	if (buffer == NULL) {
		serverFailedPerror("Error when allocating mem", sockfd);
	}

	/* Send iterative the file */
	loops = fileSize / (BUFSIZE - 1) ;
	if (fileSize % (BUFSIZE - 1)!= 0)
		loops++;
	lseek(filefd, 0, SEEK_SET);

	//printf("Loops %d\n", loops);

	while (loops--) {

		/* Take BUFSIZE bytes from file */
		readBytes = read(filefd, buffer, sizeof(char) * (BUFSIZE - 1));
		if (readBytes == -1)
			serverFailedPerror("Error when readding from file", sockfd);
		if (readBytes != BUFSIZE)
			//printf("\n\nCould only read %d\n", readBytes);
		buffer[readBytes] = '\0';

		//printf("buffer read is \n%s", buffer);

		/* Send them to the Worker */
		sendMsgToSocket(sockfd, buffer, ANSWER_TO_FETCH);
	}
	
	/* When done send EOT msg */
	sendMsgToSocket(sockfd, NULL, EOT);

	free(buffer);

	free(contentId);
	free(requestedPath);
}


void strprint(void * str) {
	char *cstr = (char *) str;
	printf("%s\n", cstr);
}


void recGetDir(const char *path, queue_t *str_q) {

	DIR * d = opendir(path);
	if (d == NULL) return; 
	struct dirent *dir;
	char *str;
	while ((dir = readdir(d)) != NULL)
    {
    	/* If we found a file add it to queue */
		if(dir-> d_type != DT_DIR) {

			str = malloc(sizeof(char) * (strlen(dir->d_name) + strlen(path) + 1));
			if (str == NULL) 
				perror_exit("Error when allocating mem");

			sprintf(str, "%s%s", path, dir->d_name);

			queue_add_node(str_q, str);
		}
		else
			/* If we found a dir add it to queue */
			if(dir -> d_type == DT_DIR && strcmp(dir->d_name,".") != 0 && strcmp(dir->d_name,"..") != 0) {

				str = malloc(sizeof(char) * (strlen(dir->d_name) + strlen(path) + 2));
				if (str == NULL) 
					perror_exit("Error when allocating mem");

				sprintf(str, "%s%s/", path, dir->d_name);
				//printf("Dir %s\n", str);
				queue_add_node(str_q, str);

				/* Call recursively */
				recGetDir(str, str_q); 
			}
	}	

	/* Close dir and exit */
	closedir(d);
}

void sendListOfContents(struct threadArg *tInfo, char *rcvmsg) {

	queue_t *string_q = queue_create();
	int     msgLen = 0, index = 0, sockfd, arraySize;
	char    **strArray, *buffer, *str, **tokens;
	const char *path;

	sockfd = tInfo->sockfd;
	path   = tInfo->dirptr;  

	/* Create queue data instance */
	queue_data *data = malloc(sizeof(queue_data));
	if (data == NULL) 
		serverFailedPerror("Error when allocating mem", sockfd);


	/* Tokenize recived msg and get content id and delay */
	tokens = threads_tokenize(rcvmsg, FLS, &arraySize);
	data->contId = tokens[0];
	data->delay  = atoi(tokens[1]);  free(tokens[1]);
	free(tokens);

	//printf("READ %s %d\n",data->contId, data->delay);
	
	/* Write the content id and delay on the queue */
	queueWrite(tInfo->conn_info, data);

	/* Initilaize buffer and str */
	buffer = malloc(sizeof(char) * BUFSIZE);
	if (buffer == NULL) 
		perror_exit("Error when allocating mem");

	str = malloc(sizeof(char) * (strlen(path) + 1));
	if (str == NULL) 
		perror_exit("Error when allocating mem");

	sprintf(str, "%s", path);
	queue_add_node(string_q, str);  // add first path to the queue

	/* Add all the dirs in the queue in the queue */
	recGetDir(path, string_q);

	/* Make an str array to hold all the containing paths */
	int size = string_q->contained_nodes;
	strArray = malloc(sizeof(char *) * (size));
	if (strArray == NULL) 
		perror_exit("Error when allocating mem");

	/* Make messages of size BUFSIZE and send them */
	int localIndex = 0;
	int emptyFlag = 0;
	int lastLoop  = 0;
	while (!emptyFlag || lastLoop) {


		/* Pop strings till we reach the size of 1024 */
		if (!emptyFlag)
			strArray[index] = (char *) queue_pop(string_q);

		emptyFlag = queue_is_empty(string_q);
		//printf("String poped :%s %d %d\n", strArray[index], emptyFlag, index);
		//printf("Message len %d\n", msgLen);

		/* If we reach the limit then send the msg Or if we emptyed the queue */
		if (lastLoop || msgLen + strlen(strArray[index]) + 1 > BUFSIZE || 
			(emptyFlag && msgLen + strlen(strArray[index]) + 1 <= BUFSIZE)) {

			/* Add also the last elemnt if it fits .If not ,lastloop would be up */
			if (emptyFlag && msgLen + strlen(strArray[index]) + 1 <= BUFSIZE && lastLoop == 0) {
				msgLen += strlen(strArray[index]) + 1;  // +1 for the sparetor of the feilds (FLS) 				
				localIndex = index + 1;                 // in case it fits , we need to add this one too, to the msg
			}
			else {
				localIndex = index;                     // Else don't add the lastly popped element to the msg
			}

			/* Copy all the string to the buffer*/
			int writenstrs = 0;
			int nextFreeSpace = 0;
			for (; writenstrs < localIndex; writenstrs++) {

				sprintf(buffer + nextFreeSpace, "%s%c", strArray[writenstrs], FLS);
				nextFreeSpace += strlen(strArray[writenstrs]) + 1;
			}
			buffer[nextFreeSpace -1] = '\0';  // replace FLS with \0 for the last string
			//printf("Buffer is %s, len %d , acc %d\n", buffer, strlen(buffer), nextFreeSpace);

			/* Send msg to MirroManager */
			sendMsgToSocket(sockfd, buffer, ATL);		

			/* Check extreme case : The last elemnt didn fit the last msg */
			/* After adding the last element make the temp falg false again */
			if (emptyFlag)
				lastLoop = 0;
			

			/* In case the last elemen of queue cant fit in , give it another loop */
			if (emptyFlag && msgLen + strlen(strArray[index]) + 1 > BUFSIZE)
				lastLoop = 1;
			/*------------------------------------------------------------*/

			/* add the last not fited to the first place */
			free(strArray[0]);
			strArray[0] = strArray[index];
			strArray[index] = NULL;
			index = 0;
			msgLen = 0;
		} 

		msgLen += strlen(strArray[index]) + 1;  // +1 for the sparetor of the feilds 
		index++;
	}	

	/* Send an EOT msg , that states the end of the answer to the LIST request */
	sendMsgToSocket(sockfd, NULL, EOT);

	/* Free and exit */
	queue_delete(string_q, NULL);

	for (index = 0; index < size; index++)
		free(strArray[index]);
	free(strArray);
	free(buffer);
	return;
}


void *connectionHandler(void *arg) {
	struct threadArg *tInfo = (struct threadArg *)arg;
	/* Cast the argument to its type */

	/* Read the msg */
	msg_t *mes = NULL;
	char  *buffer;
	buffer = readMsgFromSocket(tInfo->sockfd, &mes);

	/* LIST REQUEST */
	if (strcmp(mes->str, LSM) == 0) {
		printf("Recived LIST request\n");
		sendListOfContents(tInfo, buffer);
	}
	else if (strcmp(mes->str, FCM) == 0){
		printf("Recived FETCH request\n");
		sendRequestedContent(tInfo, buffer);
	}
	else if (strcmp(mes->str, CLOSE) == 0){
		printf("Recived CLOSE request\n");
		fflush(stdout);
		if (kill(getpid(), 15) == -1 )
			perror("Kill");
	}
	else {
		printf("Request of type :%s cant be handled\n", mes->str);
	}


	close(tInfo->sockfd);
	free(tInfo);
	free(mes);
	free(buffer);
	pthread_exit(NULL);
}