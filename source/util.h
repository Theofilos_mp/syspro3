#ifndef __UTIL_H
#define __UTIL_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "Protocol.h"

/* Content Server Info */
typedef struct tagcsInfo {
	char   *csAddress;
    char   *csPort;
    char   *csDirOrFile;
    char   *csDelay;
} csInfo_t;


void     printCsInfo    (void *csi);
void     freeCsInfo     (void *csi);
csInfo_t *string2scInfo (char *);



char  *readMsgFromSocket (int sockfd, msg_t **mes);
int   sendMsgToSocket    (int sockfd, char * msg2str, char *type);
char  **threads_tokenize (char * string, char delim, int *arraySize);


void serverFailedPerror(char * err, int sockfd);
void perror_exit(char *str);
int lenfinder(int x);


#endif


#ifdef skata

/* Create the csInfo struct */
        	csinfoptr = malloc(sizeof(CSInfo_t));
        	if (csinfoptr == NULL) {
        		perror("Error (line70)");
        		exit(-1);
        	}

        	/* tokenize string and get the info */
        	token = strtok(temp, ":");
        	printf("Token %s\n", token);

        	/* Get the address*/
        	csinfoptr->CSAddress = malloc((strlen(token) + 1) * sizeof(char));
        	if (csinfoptr->CSAddress == NULL) {perror("Error"); exit(-1);}
        	strcpy(csinfoptr->CSAddress, token);

        	/* Get the port */
        	token = strtok(NULL, ":");
        	printf("Token %s\n", token);
        	csinfoptr->CSPort = malloc((strlen(token) + 1) * sizeof(char));
        	if (csinfoptr->CSPort == NULL) {perror("Error"); exit(-1);}
        	strcpy(csinfoptr->CSPort, token);

        	/* Get the DirOdFile */
        	token = strtok(NULL, ":");
        	printf("Token %s\n", token);
        	csinfoptr->CSDirOrFile = malloc((strlen(token) + 1) * sizeof(char));
        	if (csinfoptr->CSDirOrFile == NULL) {perror("Error"); exit(-1);}
        	strcpy(csinfoptr->CSDirOrFile, token);

        	/* Get the Delay */
        	token = strtok(NULL, ",");
        	printf("Token %s\n", token);
        	csinfoptr->CSDelay = malloc((strlen(token) + 1) * sizeof(char));
        	if (csinfoptr->CSDelay == NULL) {perror("Error"); exit(-1);}
        	strcpy(csinfoptr->CSDelay, token);



        /* Add the end_of_msg char to the string we are about to send */
		if (csinfo2str[strlen(csinfo2str) - 2] == ',') {
			stringlen = strlen(csinfo2str) - 1;
			csinfo2str[strlen(csinfo2str) - 2] = '\0';			
		}
		else
			stringlen = strlen(csinfo2str);
		

		/* Send the len msg */
		msg_t msg;
		strcpy(msg.strmsg, CSI);
		msg.strmsg[3] = '\0';
		msg.lenmsg    = stringlen;

		if (write(sockfd, &msg, sizeof(msg_t)) < 0)
				perror_exit("Error when writing msg_t");

		/* Send the string */
		if (write(sockfd, csinfo2str, msg.lenmsg * sizeof(char)) < 0)
				perror_exit("Error when writing msg_t");		

		free(csinfo2str);

#endif