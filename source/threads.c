
#include "threads.h"
#include "ServerFunctions.h"

/* +----------------+
 * | INITIALIZATION |
 * |    FUNCTIONS   |   
 * +----------------+ */ 
pool_t *pool_init() {
	pool_t *pool;

	pool = malloc(sizeof(pool_t));
	if (pool == NULL) 
		perror_exit("Error when allocating mem");

	pool->start = 0;
	pool->end = -1;
	pool->count = 0;

	return pool;
}

mirrors_SyncroBlock *syncroData_init(void) {

	/* Make the block */
	mirrors_SyncroBlock *block = malloc(sizeof(mirrors_SyncroBlock));

	/* Initialize condition variable */
	pthread_mutex_init(&(block->mtx), 0);
	pthread_mutex_init(&(block->statistics_mtx), 0);
	pthread_cond_init(&(block->cond_nonfull), NULL); 
	pthread_cond_init(&(block->cond_nonempty), NULL);

	/* Intialize the pool */
	block->pool = pool_init();

	/* Initialize vars */
	block->killWorkers       = 0;
	block->bytesTransfered   = 0;
	block->filesTransfered   = 0;
	block->foldersTransfered = 0;
	block->sumOfSquares      = 0;

	return block;
}

contents_SyncroBlock *cs_syncroBlock_init(void) {

	contents_SyncroBlock *block = malloc(sizeof(contents_SyncroBlock));

	/* Intialize connection info and global vars*/
	block->delayANDid_q = queue_create();
	pthread_mutex_init(&(block->mtx), 0);
	pthread_cond_init(&(block->writers_cond), NULL); 
	pthread_cond_init(&(block->readers_cond), NULL);
	block->readers = 0;
	block->writer  = 0;

	return block;
}

void delete_queueData(void *q_data){
	queue_data *data = (queue_data *) q_data;
	free(data->contId);
	free(data);
}

void delete_cs_syncroBlock(contents_SyncroBlock *block) {

	pthread_cond_destroy(&(block->readers_cond));
	pthread_cond_destroy(&(block->writers_cond));
	pthread_mutex_destroy(&(block->mtx));

	queue_delete(block->delayANDid_q, delete_queueData);
	free(block);
}


void delete_syncroBlock(mirrors_SyncroBlock *block) {

	pthread_cond_destroy(&(block->cond_nonempty));
	pthread_cond_destroy(&(block->cond_nonfull));
	pthread_mutex_destroy(&(block->mtx));
	pthread_mutex_destroy(&(block->statistics_mtx));
	free(block->pool);
	free(block);
}

void funckillWorkers(mirrors_SyncroBlock *block) {

	/* Set kill var */
	block->killWorkers = 1;

	/* Wake up asleep workers to see the change */
	pthread_cond_broadcast(&(block->cond_nonempty));
}

void deleteCs_data(cs_data_t * ptr) {

	/* Free the id, all the others are taken from the queue that will be delete in main */
	free(ptr->cs_id);
	free(ptr->cs_dirOrFile);

	/* Free the struct */
	free(ptr);
}


/* +-------------------+
 * | PRODUCE & CONSUME |
 * |    FUNCTIONS      |   
 * +-------------------+ */
void manager_produce(cs_data_t *data, mirrors_SyncroBlock *block) {
	/* ---------------------
     * Puts a manager thread
     * in the CS
	 * --------------------- */
	 int err;

	/* Lock mutex */
	err = pthread_mutex_lock(&(block->mtx));
	if (err) { perror2("pthread_mutex_lock", err); exit(-1); }

	while ((block->pool)->count >= POOL_SIZE) {
		//printf(">>p: Found Buffer Full count: %d c:%d\n", pool->count, consumers);
		pthread_cond_wait(&(block->cond_nonfull), &(block->mtx));
	}

	/* Update shared queue */
	(block->pool)->end = ((block->pool)->end + 1) % POOL_SIZE;
	(block->pool)->data[(block->pool)->end] = data;
	(block->pool)->count++;

	/* Signal one consumer */
	pthread_cond_signal(&(block->cond_nonempty));

	err = pthread_mutex_unlock(&(block->mtx));
	if (err) { perror2("pthread_mutex_unlock", err); exit(-1); }
}

cs_data_t *worker_consume(mirrors_SyncroBlock *block) {
	/* ---------------------
     * Puts a worker thread
     * in the CS
	 * --------------------- */
	cs_data_t *data;
	int err;
	err = pthread_mutex_lock(&(block->mtx));
	if (err) { perror2("pthread_mutex_lock", err); exit(-1); }	
	while ((block->pool)->count <= 0) {
		//printf(">>c: Found Buffer Empty c: %d p:%d\n", pool->count, producers);

		/* In case we got in to worker consume bealiving that we have items to consume ,
		 * but before locking mtx other consumers before us consumed all the items .
		 * In that case return before eternal sleep.
		 */
		if (block->killWorkers == 1 && (block->pool)->count == 0) {

			/* Unlock thread and return null */
			pthread_mutex_unlock(&(block->mtx));	
			return NULL;
		}

		pthread_cond_wait(&(block->cond_nonempty),&(block->mtx));
	}

	/* Update the shared queue */
	data = (block->pool)->data[(block->pool)->start];
	(block->pool)->start = ((block->pool)->start + 1) % POOL_SIZE;
	(block->pool)->count--;

	/* Signal producer */
	pthread_cond_signal(&(block->cond_nonfull));

	pthread_mutex_unlock(&(block->mtx));
	if (err) { perror2("pthread_mutex_unlock", err); exit(-1); }

	return data;
}

/* +-------------------+
 * | HELPING FUNCTIONS |
 * +-------------------+ */
void workerTH_makeDir(char * dirFromData, char *serverPath, int notLastFlag) {
	/* ----------------------------------------------------
	 * Takes a path and create all the dirs given in path
	 * to serverPath.
	 * @notLastFlag is uses from threads that have a paht to a
	 *  filename , but want to create the path for it (if not created already)
	 * ---------------------------------------------------- */

	char **tokens;
	char *path = NULL, *prevPath;
	int  arraySize, i;

	/* Tokenize string to paths */
	tokens = threads_tokenize(dirFromData, '/', &arraySize);

	if(notLastFlag)
		arraySize--;

	/* Create prev path */
	prevPath = malloc(sizeof(char) * (strlen(serverPath) + 1));
	if (prevPath == NULL)
		perror_exit("Error when allocating mem");
	strcpy(prevPath, serverPath);

	for (i = 0; i < arraySize; i++) {

		/* Make a new string containg ourPath + newDir */
		path = malloc(sizeof(char) * (strlen(tokens[i]) + strlen(prevPath) + 2));
		if (path == NULL)
			perror_exit("Error when allocating mem");

		sprintf(path, "%s%s/", prevPath, tokens[i]);

		/* Make the dir, if it doesn t exist */
		if (mkdir(path, 0777) != 0)
			if (errno != EEXIST)
				perror_exit("Problem when makeing the dir");

		free(prevPath);
		prevPath = path;
	}

	if(arraySize == 0)
		free(prevPath);

	if(notLastFlag)
		arraySize++;

	/* Frees */
	for (i = 0; i < arraySize; i++)
		free(tokens[i]);
	free(tokens);
	free(path);
}


/* +--------------------+
 * | CONNECTION HANDLER |
 * +--------------------+ */

void *connectionHandler(void *arg) {
	struct connHandlerArgs *targ = (struct connHandlerArgs *)arg;
	/* Cast the argument to its type */
	int newsock   = targ->sockfd;
	int threadNum = targ->threadNum;
	char *dirName = targ->dirptr;


	/* Read the msg/request */
	msg_t *mes = NULL;
	char  *buffer;
	buffer = readMsgFromSocket(newsock, &mes);

	if (strcmp(mes->str, CSI) == 0) {
		/* +-------------------+
		 * | Accept Initiators |
		 * |      Request      |
		 * +-------------------+ */
		queue_t           *initiatorReqQueue = queue_create();
		mirrors_SyncroBlock *syncroBlock;
		pthread_t         *managersArray, *workersArray;
		int               arraySize = 0;

		/* Create thread synco Data block that we will pass to threads */
		syncroBlock = syncroData_init();

		/* Create workers */
		workersArray = createWorkers(threadNum, syncroBlock, dirName, newsock);

		/* Get the data */
		getCServersInfo(newsock, buffer, initiatorReqQueue);

		/* Create the MirrorManager threads */
		arraySize = initiatorReqQueue->contained_nodes;
		managersArray = createMirrorManagers(initiatorReqQueue, syncroBlock);

		/* +--------------+
		 * | Join Threads |
		 * +--------------+ */

		/* Loop all the mirror manager threads to join */
		int index = 0;
		int contentServersNotFound = 0;
		int contentServers         = 0;
		void *ret_val = NULL;
		for (; index < arraySize; index++) {

			if (pthread_join(managersArray[index], (void **) &ret_val) != 0)
				continue;

				contentServers++;
			/* In case of a erroneus return send msg to initiator */
			if (ret_val == NULL) {
			
				continue;
			}
			else if ( *((int*) ret_val) == CS_NOTFOUND) {
				contentServersNotFound++;
				sendCsNotFound(newsock, (csInfo_t *) queue_get(initiatorReqQueue, index));
			}

			free(ret_val);
		}

		/* Kill workers */
		funckillWorkers(syncroBlock);

		/* Loop all the workers threads to join */
		index = 0;
		ret_val = NULL;
		for (; index < threadNum; index++) {

			if (pthread_join(workersArray[index], (void **) &ret_val) != 0)
				continue;

			/* In case of a erroneus return send msg to initiator */
			if (ret_val == NULL) {
				continue;
			}
			else if ( *((int*) ret_val) == CS_NOTFOUND) {
				sendCsNotFound(newsock, (csInfo_t *) queue_get(initiatorReqQueue, index));
			}

			free(ret_val);
		}

		/* +-----------------+
	     * | SEND STATISTICS |
		 * +-----------------+ */
		send_statistics(newsock, syncroBlock, contentServers, contentServersNotFound);
		

		queue_delete(initiatorReqQueue, freeCsInfo);
		delete_syncroBlock(syncroBlock);
		free(managersArray);
		free(workersArray);
	}
	else if (strcmp(mes->str, LSM) == 0) {
		printf("Recived LIST request\n");
		sendListOfContents(arg, buffer);
	}
	else if (strcmp(mes->str, FCM) == 0){
		printf("Recived FETCH request\n");
		sendRequestedContent(arg, buffer);
	}
	else if (strcmp(mes->str, CLOSE) == 0){
		printf("Recived CLOSE request\n");
		fflush(stdout);
		if (kill(getpid(), 15) == -1 )
			perror("Kill");
	}
	else {
		printf("Request of type :%s cant be handled\n", mes->str);
	}


	/* Frees before exit */
	free(mes);
	free(buffer);
	close(newsock);
	free(arg);
	pthread_exit(NULL);
}



/* +-------------------+
 * | WORKER & PRODUCER |
 * | THREAD FUNCTIONS  |   
 * +-------------------+ */
void *worker(void *arg) {

	workerArgs        *warg    = (workerArgs *) arg;
	mirrors_SyncroBlock *block   = warg->syncroData;
	char              *copyDir = warg->dir;

	//printf("Workers data%p\n", block);

    /* +-----------------------+
     * | ENTER CS AND GET DATA |
     * +-----------------------+ */
    cs_data_t *data;
	while (block->killWorkers == 0 || block->pool->count > 0) {

		data = worker_consume(block);

		/* Dependign on the data it took make the right actions */
		if (data == NULL) { 
			continue;
		}
		else if (data->cs_dirOrFile[strlen(data->cs_dirOrFile) - 1] == '/') {
			/* -------------------------------------------------- 
			 * If the data it took contains a dir , dont fetch 
			 * just make the dir . (this is usend in case of asking for a dir)
			 * ------------------------------------------------- */
			workerTH_makeDir(data->cs_dirOrFile, copyDir, 0);

			/* Add 1 dir transfered */
			pthread_mutex_unlock(&(block->statistics_mtx));
			block->foldersTransfered++;
			pthread_mutex_unlock(&(block->statistics_mtx));

		}
		else {
			/* --------------------------------------------
			 * If its a file connect to server and take it 
			 * In case of error create a file containg the error
			 *-------------------------------------------- */

			 /* Before fetching file create the paht to file (in case it doesnt exists) */
			 workerTH_makeDir(data->cs_dirOrFile, copyDir, 1);

			/* +---------------------+ 
			 * | Connect with server |
             * +---------------------+ */

			int                 sockfd;
			char                *buffer;
			struct addrinfo     hints, *res;
			msg_t               *mes;

			/* Fill in some of the addrinfo fields */
			memset(&hints, 0, sizeof(hints));
			hints.ai_family = AF_INET;
			hints.ai_socktype = SOCK_STREAM;

			/* Get server's ip and port for the connect method */
			if (getaddrinfo(data->cs_ip, data->cs_port, &hints, &res) != 0) {

				/* Print error */
				perror ("Worker :Error in connect");
				
				/* Make a return code that signs missing server */
				int *ret_val = malloc(sizeof(int));
				if (ret_val == NULL) perror_exit("Error when allocating mem");
				*ret_val = CS_NOTFOUND;

				/* Join thread with the write retval */
				free(res);
				free(arg);
				pthread_exit((void *) ret_val);
			}

			/* Create the socket */
			if ((sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) < 0)
				perror_exit("Error in socket");


			/* Connect to content server */
			if (connect(sockfd, res->ai_addr, res->ai_addrlen) < 0) {

				/* Print error */
				perror ("Worker :Error in connect");
				
				/* Make a return code that signs missing server */
				int *ret_val = malloc(sizeof(int));
				if (ret_val == NULL) perror_exit("Error when allocating mem");
				*ret_val = CS_NOTFOUND;

				/* Join thread with the write retval */
				free(res);
				free(arg);
				pthread_exit((void *) ret_val);
			}

			/* Send a fetch msg with the file needed as body and content id */
			char *str = malloc(sizeof(char) * (strlen(data->cs_id) + strlen(data->cs_dirOrFile) + 2));
			if (str == NULL)
				perror_exit("Error when allocating mem");
			sprintf(str, "%s%c%s", data->cs_id, FLS, data->cs_dirOrFile);

			if (sendMsgToSocket(sockfd, str, FCM) != 0) continue;

			/* +-----------------------+
 			 * | GET FILE FROM CServer |
			 * +-----------------------+ */
	
			/* Make a new string containg ourPath + newDir */
			char *path = malloc(sizeof(char) * (strlen(data->cs_dirOrFile) + strlen(copyDir) + 1));
			if (path == NULL)
				perror_exit("Error when allocating mem");
			sprintf(path, "%s%s", copyDir, data->cs_dirOrFile);

			/* Create and Read File iterative */
			int existsFlag = 0;
			int newfd;
			int bytesTransferedForFile = 0;
			while (1) {

				/* Read each msg one by one */
				buffer = readMsgFromSocket(sockfd, &mes);

				/* Check if we got the a file transfer msg */
				if (existsFlag == 0 && strcmp(mes->str, ANSWER_TO_FETCH) == 0) {
					existsFlag  = 1;

					/* Open and create the file here */
					if ((newfd = open(path, O_CREAT |  O_WRONLY, 00777)) == -1) 
						perror_exit("Error when creating the file");
				}

				/* If we got an error or an eot */
				if (strcmp(mes->str, EOT) == 0) {

					/* When we get EOT message we dont excpect more msg , 
					 * so update the statistic */
					pthread_mutex_lock(&(block->statistics_mtx));
					/* Add 1 to files transfered */
					block->filesTransfered++;

					/* For the variance */
					block->sumOfSquares += bytesTransferedForFile * bytesTransferedForFile;

					/* Add to all bytest transfered var */
					block->bytesTransfered += bytesTransferedForFile;
					pthread_mutex_unlock(&(block->statistics_mtx));

					/* Frees */
					free(buffer);
					free(mes);

					break;
				}
				else if(strcmp(mes->str, FNN) == 0) {

					/* Send a file not found msg to content server */
					char *str = malloc(sizeof(char) * (strlen(data->cs_port) + strlen(data->cs_ip) + strlen(data->cs_dirOrFile) + 3));
					if (str == NULL)
						perror_exit("Error when allocating mem");
					sprintf(str, "%s:%s%c%s", data->cs_ip, data->cs_port, FLS, data->cs_dirOrFile);

					sendMsgToSocket(warg->initiator_fd, str, FNN);

					/* FREES */
					free(str);
					free(buffer);
					free(mes);

					/* Let worker continue his job */
					break;
				}
				else
					/* else add bytes read to bytesTransfered */
					bytesTransferedForFile += mes->len;
				

				/* Write what you took to the file */
				if (write(newfd, buffer, sizeof(char) * strlen(buffer)) == -1)
					perror_exit("Error when writing to new file");

				free(buffer);
				free(mes);
			}

			/* FREE */
			free(path);
			free(str);
			free(res);
		}

		/* Delete data */
		deleteCs_data(data);
	}

	/*Free arg*/
	free(arg);

	/* NULL TERMINATE */
	pthread_exit(NULL);
}



void *mirrorManager(void *arg) {

	/* The argument that each mirror Manager thead takes is a csInfo_t struct */
	managerArgs *marg = (managerArgs *) arg; 
	csInfo_t *csInfo   = marg->csinfo;
	mirrors_SyncroBlock *block   = marg->syncroData;

	printf("Calling the content server...%s:%s\n", csInfo->csAddress, csInfo->csPort);

	int                 sockfd, i;
	char                *msg4cs, *rcvmsg;
	struct addrinfo     hints, *res;
	msg_t               *mes;
	queue_t             *answerFromCS = queue_create();	

	/* Fill in some of the addrinfo fields */
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	/* Get server's ip nad port for the connect method */
	if (getaddrinfo(csInfo->csAddress, csInfo->csPort, &hints, &res) != 0) {
		/* Print error */
		fprintf(stderr, "Can't connect with contetn server %s:%s", csInfo->csAddress, csInfo->csPort);
		fflush(stderr);
		perror ("");
		
		/* Make a return code that signs missing server */
		int *ret_val = malloc(sizeof(int));
		if (ret_val == NULL) perror_exit("Error when allocating mem");
		*ret_val = CS_NOTFOUND;

		/* Frees before you go */
		free(arg);
		free(res);
		queue_delete(answerFromCS, NULL);

		/* Join thread with the write retval */
		pthread_exit((void *) ret_val);
	}


	/* Create the socket */
	if ((sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) < 0)
		perror_exit("Error in socket");


	/* Connect to content server */
	if (connect(sockfd, res->ai_addr, res->ai_addrlen) < 0) {

		/* Print error */
		fprintf(stderr, "Can't connect with content server %s:%s", csInfo->csAddress, csInfo->csPort);
		fflush(stderr);
		perror ("");
		
		/* Make a return code that signs missing server */
		int *ret_val = malloc(sizeof(int));
		if (ret_val == NULL) perror_exit("Error when allocating mem");
		*ret_val = CS_NOTFOUND;

		/* Frees before you go */
		free(arg);
		free(res);
		queue_delete(answerFromCS, NULL);

		/* Join thread with the write retval */
		pthread_exit((void *) ret_val);
	}

	/* make the msg */
	msg4cs = malloc(sizeof(char) * (strlen(csInfo->csAddress) + 1 + strlen(csInfo->csPort) +
									strlen(csInfo->csDelay)*2 + 3));
	if (msg4cs == NULL) perror_exit("Error when allocating mem");
	sprintf(msg4cs, "%s:%s:%s%c%s", csInfo->csAddress, csInfo->csPort, csInfo->csDelay, FLS, csInfo->csDelay);

	/* Send a LIST request */
	sendMsgToSocket(sockfd, msg4cs, LSM);

	/* Read the answer */
	while (1) {

		/* Add the answers to a queue */
		queue_add_node(answerFromCS, readMsgFromSocket(sockfd, &mes));

		/* When we get EOT message we dont excpect more msg*/
		if (strcmp(mes->str, EOT) == 0) {
			free(mes);
			break;
		}

		free(mes);
	}

	/* Close the socket */
	close (sockfd);

	/* Decide what to add on buffer and add it */
	int       arraySize = 0, firstNotFound = 1, addrDif = 0;
	char      **tokens, *found = NULL, *found_c;
	cs_data_t *consumed_data;

	while ((rcvmsg = queue_pop(answerFromCS)) != NULL) {

		/* Tokenize the each answer came from content server to the dirs or files */
		tokens = threads_tokenize(rcvmsg, FLS, &arraySize);

		/* Handle the array */
		for (i = 0; i < arraySize; i++) {

			/* Call str str on each token */
			found = strstr(tokens[i], csInfo->csDirOrFile);

			/* If we found it and its on the right hireracy level then add it to buffer */
			if (found != NULL && (firstNotFound || ( !firstNotFound && tokens[i] - found == addrDif))) {

				/* Keep the differnece bettwen the first found substring's address and its base address */
				if (firstNotFound == 1) {
					addrDif = tokens[i] - found;
					firstNotFound = 0;
				}

				/* Make a copy of found */
				found_c = malloc(sizeof(char) * (strlen(tokens[i]) + 1));
				if (found_c == NULL)
					perror_exit("Error when allocating mem");
				strcpy(found_c, tokens[i]);

				/* Add it to the buffer */
				consumed_data               = malloc(sizeof(cs_data_t));
				consumed_data->cs_ip        = csInfo->csAddress;
				consumed_data->cs_port      = csInfo->csPort;
				consumed_data->cs_dirOrFile = found_c;
				consumed_data->cs_id        = malloc(sizeof(char) * (strlen(csInfo->csAddress) + 1 + strlen(csInfo->csPort) +
									                 strlen(csInfo->csDelay) + 2));
				sprintf(consumed_data->cs_id, "%s:%s:%s", csInfo->csAddress, csInfo->csPort, csInfo->csDelay);
				
				/* Produce */
				manager_produce(consumed_data, block);
			}
		}

		/* In case not found add it to buffer and let the worker handle the errors */
		if (firstNotFound == 1) {

			char *notFound = malloc(sizeof(char) * (strlen(csInfo->csDirOrFile) + 1));
			if (notFound == NULL)
				perror_exit("error when allocating mem");
			strcpy(notFound, csInfo->csDirOrFile);

			/* Crate the data structure */
			consumed_data               = malloc(sizeof(cs_data_t));
			consumed_data->cs_ip        = csInfo->csAddress;
			consumed_data->cs_port      = csInfo->csPort;
			consumed_data->cs_dirOrFile = notFound;
			consumed_data->cs_id        = malloc(sizeof(char) * (strlen(csInfo->csAddress) + 1 + strlen(csInfo->csPort) +
								                 strlen(csInfo->csDelay) + 2));
			sprintf(consumed_data->cs_id, "%s:%s:%s", csInfo->csAddress, csInfo->csPort, csInfo->csDelay);

			/* Add to buffer */
			manager_produce(consumed_data, block);
		}

		for (i = 0; i < arraySize; i++)
			free(tokens[i]);

		/* Free array and poped string */
		free(tokens);
		free(rcvmsg);
	}

	/* Free arg and exit */
	free(arg);
	queue_delete(answerFromCS, free);
	free(msg4cs);
	free(res);
	pthread_exit(NULL);
}
