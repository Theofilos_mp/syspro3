#include "ServerFunctions.h"

/* +----------------------------+
 * |        FUNCTIONS           |
 * +----------------------------+ */
void send_statistics(int fd, mirrors_SyncroBlock *syncroBlock, int contentServers, int contentServersNotFound) {


	if (contentServers != contentServersNotFound && (syncroBlock->filesTransfered != 0 || syncroBlock->foldersTransfered != 0)) {
		char *statistics;
		int meanOfFilesBytes, meanOfSumOfSquares;

		meanOfFilesBytes   = syncroBlock->bytesTransfered / syncroBlock->filesTransfered;
		meanOfSumOfSquares = syncroBlock->sumOfSquares    / syncroBlock->filesTransfered;

		/* Variance */
		int variance = meanOfSumOfSquares - meanOfFilesBytes * meanOfFilesBytes;

		/* Create the msg */
		statistics = malloc(sizeof(char) * BUFSIZE);
		if (statistics == NULL) {
			serverFailedPerror("Error when allocating mem", fd);
		}
		sprintf(statistics, "Bytes:%d%cFiles:%d%cFolders:%d%cMean(bytes of Files):%d%cVariance(bytes of files):%d",
		                     syncroBlock->bytesTransfered, FLS, syncroBlock->filesTransfered, FLS, syncroBlock->foldersTransfered, FLS, 
		                     meanOfFilesBytes, FLS, variance);

		/* Send it */
		sendMsgToSocket(fd, statistics, TRANSFER_ENDED);
		free(statistics);
	} 
	else {

		/* Send empty statistics */
		char *statistics;

		statistics = malloc(sizeof(char) * (13));
		if (statistics == NULL) {
			serverFailedPerror("Error when allocating mem", fd);
		}
		sprintf(statistics, "NotAvailable");

		sendMsgToSocket(fd, statistics, TRANSFER_ENDED);
		free(statistics);
	}
}

void sendCsNotFound(int fd, csInfo_t *csinfoptr) {
	char *errmsg;

	msg_t msg;
	strcpy(msg.str, SNF);
	msg.str[3] = '\0';

	/* get the csinfo to send the ip:port of the server not found */
	if (csinfoptr != NULL) {
		errmsg = malloc(sizeof(char) * (strlen(csinfoptr->csAddress) + strlen(csinfoptr->csPort) + 2));
		if (errmsg == NULL)
			perror_exit("Error when allocating mem");

		sprintf(errmsg, "%s:%s", csinfoptr->csAddress, csinfoptr->csPort);
	}
	else {
		errmsg = malloc(sizeof(char) * (7 + 7 + 2));
		if (errmsg == NULL)
			perror_exit("Error when allocating mem");

		sprintf(errmsg, "%s:%s", "unknown", "unknown");	

	}	

	msg.len = strlen(errmsg) + 1;

	/* First write the pre msg */
	if (write(fd, &msg, sizeof(msg_t)) < 0)
			perror_exit("Error when writing msg_t");

	/* Then write the actual msg */
	if (write(fd, errmsg, sizeof(char) * msg.len) < 0)
			perror_exit("Error when writing msg_t");	

	free(errmsg);
}


pthread_t *createWorkers(int threadNum, mirrors_SyncroBlock *syncroBlock, char * dirName, int sockfd) {

	/* Create thread array and thread args */
	pthread_t  *workersArray = malloc(sizeof(pthread_t) * threadNum);
	int i = 0;
	if (workersArray == NULL)
		perror_exit("Error when allocating mem");

	for (; i < threadNum; i++) {

		/* Initialize and send wargs */
		workerArgs *wargs   = malloc(sizeof(workerArgs));
		if (workersArray == NULL || wargs == NULL)
			perror_exit("Error when allocating mem");
		wargs->dir          = dirName;
		wargs->initiator_fd = sockfd;
		wargs->syncroData   = syncroBlock;		
		if (pthread_create(&workersArray[i], NULL, worker, (void *) wargs));
	}

	return workersArray;
}

pthread_t *createMirrorManagers(queue_t *requestQueue, mirrors_SyncroBlock *syncroBlock) {

	/* For each node of the queue create a mirror manager thread */
	csInfo_t *csinfo;
	int err = 0, index = 0;
	pthread_t *threadArray;
	managerArgs *margs;

	threadArray = malloc(sizeof(pthread_t) * requestQueue->contained_nodes);
	if (threadArray == NULL) 
		perror_exit("Error when allocating mem");	

	while ((csinfo = (csInfo_t *) queue_get(requestQueue, index)) != NULL) {

		/* Initialize managerArgs */		
		margs = malloc(sizeof(managerArgs));
		if (margs == NULL) 
			perror_exit("error when allocating mem");
		margs->syncroData = syncroBlock;
		margs->csinfo     = csinfo;

		/* Create the thread */
		if ((err = pthread_create(threadArray + index, NULL, mirrorManager, (void *) margs)) != 0) {
			perror2("pthread_create", err);
			exit(1);
		}

		index++;
	}

	return threadArray;
}


void getCServersInfo(int sockfd, char *request, queue_t *requestQueue){
	char *token = NULL, ch = 'a';
	int index = 0, len = 0, startIndex = 0;
	csInfo_t *csiptr;

	/* Tokenize request string add add each csinfo to queue */
	 while(ch != '\0') {
	 	ch = request[index];

	 	/* In case of , and \0 we have the end of a new content server info */
		if (ch == ',' || ch == '\0') {

			token = malloc(sizeof(char) * (len + 1));
			if (token == NULL) 
				perror_exit("Error when allocating mem");

			strncpy(token, &request[startIndex], len);
			token[len] = '\0';			

			/* Proccess token and add to queue */
			csiptr = string2scInfo(token);
			queue_add_node(requestQueue, csiptr);
			
			len = -1;
			startIndex = index + 1;
			free(token);
		}

		/* Update indecies */
		index++;  len++;
	}
}



/* +---------------------------------------------------+
 * |              CONTENT SERVER FUNCTIONS             |
 * +---------------------------------------------------+ */


/* +-----------------+
 * | READER / WRITER |
 * |   FUNCTIONS     |
 * +-----------------+ */
 void queueWrite(contents_SyncroBlock *block, queue_data *data) {
 	/* --------------------------------------
 	 * This function writes to the queue the 
 	 * tuple of <id, delay> .
 	 * -------------------------------------- */

 	pthread_mutex_lock(&(block->mtx));

 	while (block->writer || block->readers >0) {
 		pthread_cond_wait(&(block->writers_cond), &(block->mtx));
 	}
 	block->writer = 1;
	pthread_mutex_unlock(&(block->mtx));

 	/* Write the data */
 	queue_add_node(block->delayANDid_q, (void *) data);

 	pthread_mutex_lock(&(block->mtx));
 	block->writer = 0;
 	pthread_cond_broadcast(&(block->readers_cond)); // wake up all readers
 	pthread_cond_signal(&(block->writers_cond));    // wake up a writer
 	pthread_mutex_unlock(&(block->mtx));

 	return;
 }

int readAndCompare(contents_SyncroBlock *block, char *contentId) {
	/* --------------------------------------
 	 * This function reads the queue and returns 
 	 * the id , if the argument contentId exists
 	 * int a tuple of the queue.
 	 * -------------------------------------- */
 	int i, ret_val = 1;  // default delay time in case it wasent found 
 	queue_data *data;

 	pthread_mutex_lock(&(block->mtx));

 	while (block->writer) {
 		pthread_cond_wait(&(block->readers_cond), &(block->mtx));
 	}
 	block->readers++;
	pthread_mutex_unlock(&(block->mtx));

 	/* Read all queue */
	for (i = 0; (data = queue_get(block->delayANDid_q, i)) != NULL; i++) {
		if (strcmp(data->contId, contentId) == 0) 
			ret_val = data->delay;
	}
 	
 	pthread_mutex_lock(&(block->mtx));
 	block->readers--;
 	if (block->readers == 0)
 		pthread_cond_signal(&(block->writers_cond));    // wake up a writer
 	pthread_mutex_unlock(&(block->mtx));

 	return ret_val;
}



/* +--------------+
 * | THREAD FUNCS |
 * +--------------+ */
void sendRequestedContent(struct connHandlerArgs *tInfo, char *rcvmsg) {
	/* --------------------------------------------
	 * Sends to the socketfd the requested info .
	 * In case of a file not found send error msg.
	 * -------------------------------------------- */
	char *buffer, *requestedPath, *contentId, **tokens;
	int  filefd, fileSize, loops, readBytes, sockfd, arraySize;

	/* Tokenize recived msg and get content id and requested file */
	tokens = threads_tokenize(rcvmsg, FLS, &arraySize);
	contentId     = tokens[0];
	requestedPath = tokens[1];
	free(tokens);

	//printf("Read %s %s\n",contentId, requestedPath);

	sockfd = tInfo->sockfd; 

	/* Check if we have that file on our server */
	if ((filefd = open(requestedPath, O_RDONLY) ) == -1 ) {
		perror("Requested file open problem");
		sendMsgToSocket(sockfd, NULL, FNN);

		free(contentId);  free(requestedPath);
		return ;
	}

	/* Wait the delay time */
	sleep(readAndCompare(tInfo->block, contentId));

	/* +-------------------------+
	 * | Send the file's content |
	 * | to mirror manager       |
	 * +-------------------------+ */
	fileSize = (int) lseek(filefd, 0, SEEK_END); 
	//printf("File has %d bytes\n", fileSize);

	/* Intialize buffer */
	buffer = malloc(sizeof(char) * BUFSIZE);
	if (buffer == NULL) {
		serverFailedPerror("Error when allocating mem", sockfd);
	}

	/* Send iterative the file */
	loops = fileSize / (BUFSIZE - 1) ;
	if (fileSize % (BUFSIZE - 1)!= 0)
		loops++;
	lseek(filefd, 0, SEEK_SET);

	//printf("Loops %d\n", loops);

	while (loops--) {

		/* Take BUFSIZE bytes from file */
		readBytes = read(filefd, buffer, sizeof(char) * (BUFSIZE - 1));
		if (readBytes == -1)
			serverFailedPerror("Error when readding from file", sockfd);
		if (readBytes != BUFSIZE)
			//printf("\n\nCould only read %d\n", readBytes);
		buffer[readBytes] = '\0';

		//printf("buffer read is \n%s", buffer);

		/* Send them to the Worker */
		sendMsgToSocket(sockfd, buffer, ANSWER_TO_FETCH);
	}
	
	/* When done send EOT msg */
	sendMsgToSocket(sockfd, NULL, EOT);

	free(buffer);

	free(contentId);
	free(requestedPath);
}


void strprint(void * str) {
	char *cstr = (char *) str;
	printf("%s\n", cstr);
}


void recGetDir(const char *path, queue_t *str_q) {

	DIR * d = opendir(path);
	if (d == NULL) return; 
	struct dirent *dir;
	char *str;
	while ((dir = readdir(d)) != NULL)
    {
    	/* If we found a file add it to queue */
		if(dir-> d_type != DT_DIR) {

			str = malloc(sizeof(char) * (strlen(dir->d_name) + strlen(path) + 1));
			if (str == NULL) 
				perror_exit("Error when allocating mem");

			sprintf(str, "%s%s", path, dir->d_name);

			queue_add_node(str_q, str);
		}
		else
			/* If we found a dir add it to queue */
			if(dir -> d_type == DT_DIR && strcmp(dir->d_name,".") != 0 && strcmp(dir->d_name,"..") != 0) {

				str = malloc(sizeof(char) * (strlen(dir->d_name) + strlen(path) + 2));
				if (str == NULL) 
					perror_exit("Error when allocating mem");

				sprintf(str, "%s%s/", path, dir->d_name);
				//printf("Dir %s\n", str);
				queue_add_node(str_q, str);

				/* Call recursively */
				recGetDir(str, str_q); 
			}
	}	

	/* Close dir and exit */
	closedir(d);
}

void sendListOfContents(struct connHandlerArgs *tInfo, char *rcvmsg) {

	queue_t *string_q = queue_create();
	int     msgLen = 0, index = 0, sockfd, arraySize;
	char    **strArray, *buffer, *str, **tokens;
	const char *path;

	sockfd = tInfo->sockfd;
	path   = tInfo->dirptr;  

	/* Create queue data instance */
	queue_data *data = malloc(sizeof(queue_data));
	if (data == NULL) 
		serverFailedPerror("Error when allocating mem", sockfd);


	/* Tokenize recived msg and get content id and delay */
	tokens = threads_tokenize(rcvmsg, FLS, &arraySize);
	data->contId = tokens[0];
	data->delay  = atoi(tokens[1]);  free(tokens[1]);
	free(tokens);

	//printf("READ %s %d\n",data->contId, data->delay);
	
	/* Write the content id and delay on the queue */
	queueWrite(tInfo->block, data);

	/* Initilaize buffer and str */
	buffer = malloc(sizeof(char) * BUFSIZE);
	if (buffer == NULL) 
		perror_exit("Error when allocating mem");

	str = malloc(sizeof(char) * (strlen(path) + 1));
	if (str == NULL) 
		perror_exit("Error when allocating mem");

	sprintf(str, "%s", path);
	queue_add_node(string_q, str);  // add first path to the queue

	/* Add all the dirs in the queue in the queue */
	recGetDir(path, string_q);

	/* Make an str array to hold all the containing paths */
	int size = string_q->contained_nodes;
	strArray = malloc(sizeof(char *) * (size));
	if (strArray == NULL) 
		perror_exit("Error when allocating mem");

	/* Make messages of size BUFSIZE and send them */
	int localIndex = 0;
	int emptyFlag = 0;
	int lastLoop  = 0;
	while (!emptyFlag || lastLoop) {


		/* Pop strings till we reach the size of 1024 */
		if (!emptyFlag)
			strArray[index] = (char *) queue_pop(string_q);

		emptyFlag = queue_is_empty(string_q);
		//printf("String poped :%s %d %d\n", strArray[index], emptyFlag, index);
		//printf("Message len %d\n", msgLen);

		/* If we reach the limit then send the msg Or if we emptyed the queue */
		if (lastLoop || msgLen + strlen(strArray[index]) + 1 > BUFSIZE || 
			(emptyFlag && msgLen + strlen(strArray[index]) + 1 <= BUFSIZE)) {

			/* Add also the last elemnt if it fits .If not ,lastloop would be up */
			if (emptyFlag && msgLen + strlen(strArray[index]) + 1 <= BUFSIZE && lastLoop == 0) {
				msgLen += strlen(strArray[index]) + 1;  // +1 for the sparetor of the feilds (FLS) 				
				localIndex = index + 1;                 // in case it fits , we need to add this one too, to the msg
			}
			else {
				localIndex = index;                     // Else don't add the lastly popped element to the msg
			}

			/* Copy all the string to the buffer*/
			int writenstrs = 0;
			int nextFreeSpace = 0;
			for (; writenstrs < localIndex; writenstrs++) {

				sprintf(buffer + nextFreeSpace, "%s%c", strArray[writenstrs], FLS);
				nextFreeSpace += strlen(strArray[writenstrs]) + 1;
			}
			buffer[nextFreeSpace -1] = '\0';  // replace FLS with \0 for the last string
			//printf("Buffer is %s, len %d , acc %d\n", buffer, strlen(buffer), nextFreeSpace);

			/* Send msg to MirroManager */
			sendMsgToSocket(sockfd, buffer, ATL);		

			/* Check extreme case : The last elemnt didn fit the last msg */
			/* After adding the last element make the temp falg false again */
			if (emptyFlag)
				lastLoop = 0;
			

			/* In case the last elemen of queue cant fit in , give it another loop */
			if (emptyFlag && msgLen + strlen(strArray[index]) + 1 > BUFSIZE)
				lastLoop = 1;
			/*------------------------------------------------------------*/

			/* add the last not fited to the first place */
			free(strArray[0]);
			strArray[0] = strArray[index];
			strArray[index] = NULL;
			index = 0;
			msgLen = 0;
		} 

		msgLen += strlen(strArray[index]) + 1;  // +1 for the sparetor of the feilds 
		index++;
	}	

	/* Send an EOT msg , that states the end of the answer to the LIST request */
	sendMsgToSocket(sockfd, NULL, EOT);

	/* Free and exit */
	queue_delete(string_q, NULL);

	for (index = 0; index < size; index++)
		free(strArray[index]);
	free(strArray);
	free(buffer);
	return;
}
