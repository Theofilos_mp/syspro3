CC			= gcc
CFLAGS      = -Wall -c
UTILSRCS    = source/util.c source/queue.c
UTILOBJS    = $(UTILSRCS:.c=.o)
SRCS1       = source/MirrorServer.c source/threads.c source/ServerFunctions.c
OBJS1       = $(SRCS1:.c=.o)
SRCS2       = source/MirrorInitiator.c
OBJS2       = $(SRCS2:.c=.o)
SRCS3       = source/ContentServer.c
OBJS3       = $(SRCS3:.c=.o)

EXEC1       = MirrorServer
EXEC2       = MirrorInitiator
EXEC3       = ContentServer


all: $(EXEC1) $(EXEC2) $(EXEC3)


$(EXEC1): $(OBJS1) $(UTILOBJS)
	$(CC)  $(OBJS1) $(UTILOBJS) -o $@ -pthread

$(EXEC2): $(OBJS2) $(UTILOBJS)
	$(CC)  $(OBJS2) $(UTILOBJS) -o $@

$(EXEC3): $(OBJS3) $(UTILOBJS)
	$(CC)  $(OBJS3) $(UTILOBJS) -o $@ -pthread

.o: .c
	(CC) $(CFLAGS) $< -o $@
	

.PHONE = clean
clean :	
	rm -f $(OBJS1) $(OBJS2) $(OBJS3) $(UTILOBJS) $(EXEC1) $(EXEC2) $(EXEC3)